#include "type.h"


Type::Type()
{
	_isTemp = false;
}

bool Type::is_temp() const
{
	return _isTemp;
}
void Type::set_temp(bool isTemp)
{
	_isTemp = isTemp;
}
