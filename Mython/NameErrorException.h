#pragma once

#include "InterperterException.h"
#include <string>


class NameErrorException : public InterperterException
{
	std::string _name;
	char* _c_str;
public:
	NameErrorException(const std::string& name);
	~NameErrorException();

	virtual const char* what() const throw();
};
