#include "NameErrorException.h"
#include <string>


NameErrorException::NameErrorException(const std::string& name)
{
	_name = name;

	std::string s = ("NameError : name '" + _name + "' is not defined");
	_c_str = new char[s.size() + 1];
	memcpy_s(_c_str, s.size() + 1, s.c_str(), s.size() + 1);
}
NameErrorException::~NameErrorException()
{
	delete[] _c_str;
}

const char* NameErrorException::what() const throw()
{
	return _c_str;
}
