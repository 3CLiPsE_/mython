#include "parser.h"
#include <iostream>
#include "IndentationException.h"
#include "type.h"
#include "Helper.h"
#include "Integer.h"
#include "Boolean.h"
#include "String.h"
#include "SyntaxException.h"
#include "Void.h"
#include "NameErrorException.h"


std::unordered_map<std::string, Type*> Parser::_variables;

bool Parser::isLegalVarName(const std::string& str)
{
	if (Helper::isDigit(str[0]))
		return false;
	for (char c : str.substr(1))
		if (!Helper::isDigit(c) && !Helper::isLetter(c) && !Helper::isUnderscore(c))
			return false;
	return true;
}
bool Parser::makeAssignment(const std::string& str)
{
	if (std::count(str.begin(), str.end(), '=') != 1)
		return false;
	std::string name = str.substr(0, str.find('='));	// start to '='
	std::string valueString = str.substr(str.find('=') + 1);	// '=' to end (not including '=')
	Helper::rtrim(name);
	Helper::trim(valueString);
	if (!isLegalVarName(name))
		throw NameErrorException(name);
	Type* value = getType(valueString);
	if (value == nullptr)
	{
		if (isLegalVarName(valueString))
		{
			value = getVariableValue(valueString);
			if (value == nullptr)
				throw NameErrorException(valueString);
			else
			{
				_variables.erase(name);		// make sure dtor is called
				_variables.insert({ name, value->clone() });
				return true;
			}
		}
		else
			throw SyntaxException();
	}

	_variables.erase(name);		// make sure dtor is called
	_variables.insert({ name, value });

	return true;
}
Type* Parser::getVariableValue(const std::string &str)
{
	auto varIt = _variables.find(str);
	if (varIt == _variables.end())
		return nullptr;
	return varIt->second;
}

Type* Parser::parseString(const std::string& str)
{
	if (str.length() > 0)
	{
		if (str[0] == ' ' || str[0] == '\t')
			throw IndentationException();
		std::string toParse = str;
		Helper::rtrim(toParse);
		Type* parsed;
		if (isLegalVarName(toParse))
		{
			parsed = getVariableValue(toParse);
			if (parsed != nullptr)
			{
				parsed->set_temp(false);
				return parsed;
			}
			else
				throw NameErrorException(toParse);
		}
		parsed = getType(toParse);
		if (parsed != nullptr)
			return parsed;
		else
			if (makeAssignment(toParse))
			{
				Void* v = new Void();	// returning nothing (empty line)
				v->set_temp(true);
				return v;
			}
		throw SyntaxException();
	}
	return nullptr;
}
Type* Parser::getType(const std::string& str)
{
	Type* toReturn = nullptr;
	if (Helper::isInteger(str))
	{
		toReturn = new Integer(std::stoi(str));
	}
	else if (Helper::isBoolean(str))
	{
		toReturn = new Boolean(str == "True");
	}
	else if (Helper::isString(str))
	{
		toReturn = new String(str.substr(1, str.size() - 2));
	}
	if (toReturn != nullptr)
		toReturn->set_temp(true);
	return toReturn;
}
void Parser::clearMemory()
{
	_variables.clear();		// all destructors are called, and map is emptied.
}
