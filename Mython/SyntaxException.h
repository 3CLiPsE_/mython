#pragma once

#include "InterperterException.h"


class SyntaxException : public InterperterException
{
	virtual const char* what() const throw();
};
