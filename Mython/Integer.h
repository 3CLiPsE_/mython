#pragma once

#include "type.h"
#include <string>


class Integer : public Type
{
	int _value;
public:
	Integer(int num);

	virtual Integer* clone();

	virtual bool is_printable() const;
	virtual std::string to_string() const;
};
