#pragma once

#include "type.h"
#include <string>


class Boolean : public Type
{
	bool _value;
public:
	Boolean(bool value);

	virtual Boolean* clone();

	virtual bool is_printable() const;
	virtual std::string to_string() const;
};
