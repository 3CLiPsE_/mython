#pragma once

#include "Sequence.h"
#include <string>


class String : public Sequence
{
	std::string _value;
public:
	String(const std::string& value);

	virtual String* clone();

	virtual bool is_printable() const;
	virtual std::string to_string() const;
};
