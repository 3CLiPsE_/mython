#include "Boolean.h"


Boolean::Boolean(bool value)
{
	_value = value;
}

Boolean* Boolean::clone()
{
	Boolean* b = new Boolean(_value);
	b->set_temp(is_temp());
	return b;
}

bool Boolean::is_printable() const
{
	return true;
}
std::string Boolean::to_string() const
{
	return _value ? "True" : "False";
}
