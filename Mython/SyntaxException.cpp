#include "SyntaxException.h"


const char* SyntaxException::what() const throw()
{
	return "SyntaxError: Invalid Syntax";
}
