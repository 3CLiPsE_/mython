#include "String.h"


String::String(const std::string& value)
{
	_value = value;
}

String* String::clone()
{
	String* s = new String(_value);
	s->set_temp(is_temp());
	return s;
}

bool String::is_printable() const
{
	return true;
}
std::string String::to_string() const
{
	if (_value.find("'") != std::string::npos)
	{
		return "\"" + _value + "\"";
	}
	else
	{
		return "'" + _value + "'";
	}
}
