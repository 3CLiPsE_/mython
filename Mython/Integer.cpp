#include "Integer.h"


Integer::Integer(int num)
{
	_value = num;
}

Integer* Integer::clone()
{
	Integer* i = new Integer(_value);
	i->set_temp(is_temp());
	return i;
}

bool Integer::is_printable() const
{
	return true;
}
std::string Integer::to_string() const
{
	return std::to_string(_value);
}
