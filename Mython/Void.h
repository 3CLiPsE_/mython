#pragma once

#include "type.h"
#include <string>


class Void : public Type
{
public:
	Void();
	
	virtual Void* clone();

	virtual bool is_printable() const;
	virtual std::string to_string() const;
};
