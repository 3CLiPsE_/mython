#include "type.h"
#include "InterperterException.h"
#include "parser.h"
#include <iostream>

#define WELCOME "Welcome to Magshimim Python Interperter version 1.0 by "
#define YOUR_NAME "Shahak \"3CLiPsE\" Kozlovski"


int main(int argc, char **argv)
{
	std::cout << WELCOME << YOUR_NAME << std::endl;

	std::string input_string;

	// get new command from user
	std::cout << ">>> ";
	std::getline(std::cin, input_string);

	while (input_string != "quit()")
	{
		// prasing command
		try
		{
			const Type* value = Parser::parseString(input_string);
			if (value != nullptr)
			{
				if (value->is_printable())
					std::cout << value->to_string() << std::endl;
				if (value->is_temp())
					delete value;
			}
		}
		catch (InterperterException& e)
		{
			std::cerr << e.what() << std::endl;
		}

		// get new command from user
		std::cout << ">>> ";
		std::getline(std::cin, input_string);
	}

	Parser::clearMemory();

	return 0;
}
