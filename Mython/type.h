#pragma once

#include <string>


class Type
{
	bool _isTemp;
public:
	Type();
	
	virtual Type* clone() = 0;

	bool is_temp() const;
	void set_temp(bool isTemp);

	virtual bool is_printable() const = 0;
	virtual std::string to_string() const = 0;
};
