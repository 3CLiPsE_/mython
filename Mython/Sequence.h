#pragma once

#include "type.h"


class Sequence : public Type
{
public:
	Sequence();

	virtual Sequence* clone() = 0;

	virtual bool is_printable() const = 0;
	virtual std::string to_string() const = 0;
};
